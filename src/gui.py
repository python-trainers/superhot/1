from trainerbase.gui import CodeInjectionUI, add_components, simple_trainerbase_menu

from injections import infinite_ammo


@simple_trainerbase_menu("SUPERHOT", 250, 100)
def run_menu():
    add_components(CodeInjectionUI(infinite_ammo, "Infinite Ammo", "F1"))
