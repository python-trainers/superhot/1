from trainerbase.codeinjection import AllocatingCodeInjection


infinite_ammo = AllocatingCodeInjection(
    "48 63 86 78 01 00 00 F3 0F 2A C0 F3 0F 5A C0",
    """
        mov dword [rsi + 0x178], 99

        movsxd rax, dword [rsi + 0x178]
        cvtsi2ss xmm0, eax
        cvtss2sd xmm0, xmm0
    """,
    original_code_length=15,
)
