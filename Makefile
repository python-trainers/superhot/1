run:
	poetry run python src/main.py

lint: format
	poetry run pylint src

format: sort_imports
	poetry run black src

sort_imports:
	poetry run isort src
